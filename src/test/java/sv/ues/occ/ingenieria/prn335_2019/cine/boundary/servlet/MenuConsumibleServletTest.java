/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.ues.occ.ingenieria.prn335_2019.cine.boundary.servlet;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.AssertTrue;
import javax.ws.rs.core.Response;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import sv.ues.occ.ingenieria.prn335_2019.cine.control.MenuConsumibleBean;
import sv.ues.occ.ingenieria.prn335_2019.cine.entity.MenuConsumible;

/**
 *
 * @author jcpenya
 */
@ExtendWith(MockitoExtension.class)
public class MenuConsumibleServletTest {

    public MenuConsumibleServletTest() {

    }

    public String getCarnet() {
        return null;
    }

    @Test
    public void testDoGet() throws Exception {
        System.out.println("doGet");
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        MenuConsumibleServlet instance = new MenuConsumibleServlet();

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        Mockito.when(response.getWriter()).thenReturn(pw);
        instance.doGet(request, response);

        //fail("The test case is a prototype.");
    }

    @Test
    public void testDoPost() throws Exception {
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        MenuConsumibleServlet instance = new MenuConsumibleServlet();
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        Mockito.when(response.getWriter()).thenReturn(pw);
        instance.doPost(request, response);
        // fail("The test case is a prototype.");
    }

    @Test
    public void testGetServletInfo() {
        System.out.println("getServletInfo");
        MenuConsumibleServlet instance = new MenuConsumibleServlet();
        String expResult = "Short description";
        String result = instance.getServletInfo();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    @Test
    public void testProcessRequest() throws Exception {
        System.out.println("processRequest");

        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);

        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        Map mockMap = Mockito.mock(Map.class);
        Mockito.when(request.getParameterMap()).thenReturn(mockMap);
        Mockito.when(request.getParameterMap().containsKey("nombre")).thenReturn(Boolean.TRUE);

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);

        Mockito.when(response.getWriter()).thenReturn(pw);
        Mockito.when(request.getParameterMap().containsKey("nombre")).thenReturn(Boolean.FALSE);

        MenuConsumibleServlet instance = new MenuConsumibleServlet();
        instance.processRequest(request, response);

        pw.flush();

        MenuConsumibleBean mockBean = Mockito.mock(MenuConsumibleBean.class);

        Mockito.when(request.getParameterMap().containsKey("nombre")).thenReturn(Boolean.TRUE);
        instance.menuConsumibleBean = mockBean;//

        Mockito.when(request.getParameter("nombre")).thenReturn("nombre");
        Mockito.when(mockBean.countByNombreLike(Mockito.anyString())).thenReturn(1);
        instance.processRequest(request, response);
        
        MenuConsumible mockM = Mockito.mock(MenuConsumible.class);
        
        Mockito.when(mockBean.countByNombreLike(Mockito.anyString())).thenReturn(0);
        instance.processRequest(request, response);
        
        
        

         
        
        
        


    }

}
